package learnSpring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "learnSpring.controller" }) //Need to Scan and bind jsp pages referenced in controller
public class WebConfig implements  WebMvcConfigurer {
	
	 @Override
	   public void configureViewResolvers(ViewResolverRegistry registry) {
	      registry.jsp().prefix("/WEB-INF/pages/").suffix(".jsp");
	   }

}
