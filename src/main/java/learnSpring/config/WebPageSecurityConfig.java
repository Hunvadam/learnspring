package learnSpring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class WebPageSecurityConfig extends WebSecurityConfigurerAdapter{
	
	
	   @Override //TODO THIS IS NOT SAFE ONLY PLACEHOLDER
	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	      auth.inMemoryAuthentication()
	         .withUser("sunil").password("{noop}pass123").roles("USER")
	         .and()
	         .withUser("admin").password("{noop}pass123").roles("ADMIN");
	   }
	   
	   @Override
	   protected void configure(HttpSecurity http) throws Exception {
	      http.authorizeRequests().antMatchers("/").permitAll()
	      .and()
	      .authorizeRequests().antMatchers("/user**").hasRole("USER")
	      .and()
	      .authorizeRequests().antMatchers("/admin**").hasRole("ADMIN")
	      .and()
	      .formLogin()
	      .and()
	      .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
	   }

}
