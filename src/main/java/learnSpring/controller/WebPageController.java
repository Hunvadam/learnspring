package learnSpring.controller;

import java.security.Principal;

//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebPageController {
	 @GetMapping("/")
	    public String index(Model model, String name) {
	        return "index";
	    }
	
	 @GetMapping("/hello") //hello() /hello?name=adam 
	    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
	        model.addAttribute("name", name); //hello.jsp
	        return "hello";
	    }
	 
	 @GetMapping("/login")
	    public String login(Model model, String name) {
	        return "login";
	    }
	 
	   @GetMapping("/user")
	   public String user(Model model, Principal principal) { //principal is currently logged in/authenticated user
	      System.out.println(principal.getName());
	      return "user";
	   }
	   
	   @GetMapping("/admin")
	   public String admin(Model model, Principal principal) {
		   System.out.println(principal.getName());
	      return "admin";
	   }
	   //alternate way using SecurityContext
		  /* @GetMapping("/admin")
	   public String admin() {
	      SecurityContext context = SecurityContextHolder.getContext();
	      System.out.println(context.getAuthentication().getName());
	      return "admin";
	   }*/
}
